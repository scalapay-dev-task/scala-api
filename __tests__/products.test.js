const request = require('supertest')
const app = require('../app')

let server;

beforeEach(async () => {
	server = await app.listen(4000);
	global.agent = request.agent(server);
});

afterEach(async () => {
	await server.close();
});

describe('Products Endpoints', () =>
{
	it('should get all products', async () => {
		const res = await global.agent.get('/products');

		expect(res.statusCode).toEqual(200);
		expect(res.body).toEqual(expect.any(Array));
		expect(res.body.length).toBe(20);
	})

	it('should get only one product', async () => {
		const res = await global.agent.get('/products?q=T-shirts');

		expect(res.statusCode).toEqual(200);
		expect(res.body).toEqual(expect.any(Array));
		expect(res.body.length).toBe(1);
		expect(res.body[0].id).toBe(2);
	})
})
