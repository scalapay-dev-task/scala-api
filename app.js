const express = require('express')
const app = express()
const cors = require('cors')
const helmet = require("helmet");
const winston = require('winston'), expressWinston = require('express-winston');
const { combine, timestamp, printf, colorize, align } = winston.format;

const data = require('./products.json')

app.use(express.json());
app.use(cors())
app.use(helmet());
app.use(expressWinston.logger({
	transports: [
		new winston.transports.Console()
	],
	format: combine(
		colorize({ all: true }),
		timestamp({
			format: 'YYYY-MM-DD hh:mm:ss.SSS A',
		}),
		align(),
		printf((info) => `[${info.timestamp}] ${info.level}: ${info.message}`)
	),
	meta: false,
	colorize: true
}));

app.get('/products', (req, res) => {
	const { q } = req.query;

	if(!q) {
		setTimeout(() => res.json(data), Math.floor(Math.random() * 500) + 200);
	}
	else {
		const regex = new RegExp(q.toLowerCase(), 'gm');
		const responseData = data.filter(el => el.title.toLowerCase().search(regex) !== -1);

		setTimeout(() => res.json(responseData), Math.floor(Math.random() * 500) + 200);
	}
})

module.exports = app
