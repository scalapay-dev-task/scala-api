const app = require('./app.js')
const PORT = process.env.PORT || 4000

app.listen(PORT, () => console.log(`Scala API listening on port: ${PORT}`))

