# Getting Started with Scala API

## Available Scripts

In the project directory, you can run:

### `npm serve`

Runs the app.\
Open [http://localhost:4000/products](http://localhost:4000/products) to view products available.

It's also possible to search by name appneding the 'q' param to the url.

### `npm test`

Launches the test runner.
